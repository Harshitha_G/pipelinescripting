stage('Job Input'){

    node{

        properties(

            [parameters(

                [string( name: 'JobName')]

                )

            ]
 
       )

    }

}

stage('Notify'){

    node{

        def job=Jenkins.instance.getItem("${params.JobName}")

        def CurrentBuildResult=job.getLastBuild().getResult()        
        	
	def PreviousBuildResult=job.getLastBuild().getPreviousBuild().getResult()
        
	if("${PreviousBuildResult}"=="FAILURE" && "${CurrentBuildResult}"=="SUCCESS" ){

            println "Job '${params.JobName}' Build '${job.getNextBuildNumber()-1}'  is successful "
            println "made chnages in new branch for testing"

        }
    }

}


